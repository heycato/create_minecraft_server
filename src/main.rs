extern crate dialoguer;

use dialoguer::{Confirmation, theme::ColorfulTheme, Input, Select};

use std::collections::HashMap;
use std::fs;
use std::process;

macro_rules! map(
    { $($key:expr => $value:expr),+ } => {
        {
            let mut m = ::std::collections::HashMap::new();
            $(
                m.insert($key, $value);
            )+
            m
        }
    };
);

fn main() {
    let difficulties = vec![ "Peaceful", "Easy", "Normal", "Hard" ];
    let gamemodes = vec![ "Survival", "Creative", "Adventure", "Spectator" ];
    let leveltypes = vec![ "Amplified", "Buffet", "Flat", "LargeBiomes", "Default"];

    let bin_filenames = get_file_list("./bin");
    if bin_filenames.len() < 1 {
        println!("");
        println!("Sorry, this utility is unable to build a server.");
        println!("");
        println!("  PROBLEM: There are no files in ./bin directory.");
        println!("  SOLUTION: Place server binary (jar) files in the ./bin directory.");
        println!("");
        println!("Good bye.");
        println!("");
        process::exit(1);
    }

    let server_bin_files = vec_string_to_str(&bin_filenames);

    // prompt for name of directory for new server
    let directory_name =
        prompt_input("Enter a name for your server directory", "server");

    // prompt selection of server bin file
    let server_selection =
        prompt_selection("Select a server file to build", &server_bin_files, 0);
    let bin_filename = server_bin_files[server_selection.unwrap()];

    // prompt selection of server difficulty
    let difficulty_selection =
        prompt_selection("Select difficulty", &difficulties, 1);
    let difficulty_str = match difficulty_selection {
        Some(result) => result.to_string(),
        None => panic!("an error occurred getting difficulty"),
    };

    // prompt selection of server gamemode
    let gamemode_selection =
        prompt_selection("Select game mode", &gamemodes, 0);
    let gamemode_str = match gamemode_selection {
        Some(result) => result.to_string(),
        None => panic!("an error occurred getting gamemode"),
    };

    // prompt selection of server leveltype
    let leveltype_selection =
        prompt_selection("Select level type", &leveltypes, 4);

    let leveltype_str = match leveltype_selection {
        Some(n) => {
            match n {
                0 => "AMPLIFIED",
                1 => "BUFFET",
                2 => "FLAT",
                3 => "LARGEBIOMES",
                _ => "DEFAULT",
            }
        }
        None => "DEFAULT"
    };

    // shadow leveltype to string
    let leveltype_str = leveltype_str.to_string();

    // prompt input for server creation seed
    let levelseed =
        prompt_input("Enter a custom seed", "");

    // prompt input for message of the day
    let motd =
        prompt_input("Enter a display name for your server", "A Minecraft Server");

    if prompt_confirm("Would you like to build this server?") {
        let server_properties = build_server_properties(map! {
            "difficulty" => difficulty_str,
            "gamemode" => gamemode_str,
            "level-type" => leveltype_str,
            "level-seed" => levelseed,
            "motd" => motd
        });
        build_server(&directory_name, bin_filename, server_properties);
        print_exit_message(&directory_name);
    } else {
        println!("Build was cancelled.");
    }
    println!("Good bye.");
}

fn build_eula_file(dir: &str) {
    fs::write(&format!("{}/eula.txt", dir), "eula=true")
        .expect("unable to write file: eula.txt");
}

fn build_property_file(dir: &str, props: HashMap<&str, String>) {
    let mut properties_output = String::new();

    for (key, value) in props {
        properties_output.push_str(&format!("{}={}\n", key, value));
    }

    fs::write(&format!("{}/server.properties", dir), properties_output)
        .expect("unable to write file: server.properties");
}

fn build_server(dir: &str, bin: &str, props: HashMap<&str, String>) {
    fs::create_dir(&dir)
        .expect("Error creating server directory");

    build_property_file(&dir, props);
    build_eula_file(&dir);

    fs::copy("start_server.sh", &format!("{}/start_server.sh", &dir))
        .expect("Error moving start_server.sh");

    fs::copy(&format!("bin/{}", bin), &format!("{}/server.jar", &dir))
        .expect("Error moving bin file");
}

fn build_server_properties<'a>(edited_props: HashMap<&'a str, String>) -> HashMap<&'a str, String> {
    let server_props = map!{
        "allow-nether" => "true".to_string(),
        "allow-flight" => "false".to_string(),
        "enable-command-block" => "false".to_string(),
        "enable-query" => "false".to_string(),
        "enable-rcon" => "false".to_string(),
        "enforce-whitelist" => "false".to_string(),
        "force-gamemode" => "false".to_string(),
        "generate-structures" => "true".to_string(),
        "generator-settings" => "".to_string(),
        "level-name" => "world".to_string(),
        "hardcore" => "false".to_string(),
        "max-build-height" => "256".to_string(),
        "max-players" => "20".to_string(),
        "max-tick-time" => "60000".to_string(),
        "max-world-size" => "29999984".to_string(),
        "network-compression-threshold" => "256".to_string(),
        "online-mode" => "true".to_string(),
        "ops-permissions-level" => "4".to_string(),
        "player-idle-timeout" => "0".to_string(),
        "prevent-proxy-connections" => "false".to_string(),
        "pvp" => "true".to_string(),
        "resource-pack" => "".to_string(),
        "resource-pack-sha1" => "".to_string(),
        "server-port" => "25565".to_string(),
        "server-ip" => "".to_string(),
        "spawn-animals" => "true".to_string(),
        "spawn-monsters" => "true".to_string(),
        "spawn-npcs" => "true".to_string(),
        "snooper-enabled" => "false".to_string(),
        "view-distance" => "10".to_string(),
        "white-list" => "false".to_string()
    };
    server_props
        .into_iter()
        .chain(edited_props)
        .collect()
}

fn get_file_list(path: &str) -> Vec<String> {
    fs::read_dir(path)
        .unwrap()
        .map(|entry| {
            let entry = entry.unwrap();
            let entry_path = entry.path();
            let file_name = entry_path.file_name().unwrap();
            let file_name_as_str = file_name.to_str().unwrap();
            let file_name_as_string = String::from(file_name_as_str);
            file_name_as_string
        })
        .collect::<Vec<String>>()
}

fn print_exit_message(dir: &str) {
    println!("Build complete!");
    println!("");
    println!("  To run your new server:");
    println!("      Enter your new server directory: `cd {}`", dir);
    println!("      Run: `./start_server.sh`");
}

fn prompt_confirm(text: &str) -> bool {
    Confirmation::new()
        .with_text(text)
        .interact()
        .unwrap()
}

fn prompt_input(text: &str, default_input: &str) -> String {
    Input::new()
        .with_prompt(text)
        .default(String::from(default_input))
        .show_default(true)
        .interact()
        .unwrap()
}

fn prompt_selection(text: &str, options:&Vec<&str>, default_selection:usize) -> Option<usize> {
    let output =
        Select::with_theme(&ColorfulTheme::default())
            .with_prompt(text)
            .default(default_selection)
            .items(options.as_slice())
            .interact()
            .unwrap();
    Some(output)
}

fn vec_string_to_str<'a>(vs:&'a Vec<String>) -> Vec<&'a str> {
    vs
        .iter()
        .map(|s| &**s)
        .collect()
}
